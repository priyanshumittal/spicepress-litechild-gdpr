<?php
add_action('wp_enqueue_scripts', 'spicepress_gdpr_child_theme_css', 999);
function spicepress_gdpr_child_theme_css() {
    wp_enqueue_style('spicepress-child-parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style('media-responsive-css', get_stylesheet_directory_uri()."/css/media-responsive.css" );
	wp_enqueue_style ('spicepress-font-css',get_stylesheet_directory_uri() .'/css/font/font.css'); // 
	wp_dequeue_style( 'spicepress-fonts', spicepress_fonts_url(), array(), null );
}
?>